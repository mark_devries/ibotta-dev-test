package com.ibotta.ibottadevtest.respository;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.repository.OffersRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

public class OffersRepositoryTest {

    private static final String OFFERS_JSON = "Offers.json";
    private static final String FAKE_JSON = "Fake.json";
    private static final String FIRST_OFFER_ID = "110579";

    @Mock
    Context context;
    @Mock
    AssetManager assetManager;

    Gson gson = new Gson();

    URL resource;

    OffersRepository offersRepository = null;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);//create all @Mock objetcs
        doReturn(assetManager).when(context).getAssets();
        resource = OffersRepositoryTest.class.getClassLoader().getResource(OFFERS_JSON);
        InputStream inputStream = new FileInputStream(resource.getPath());
        doThrow(IOException.class).when(assetManager).open(FAKE_JSON);
        doReturn(inputStream).when(assetManager).open(resource.getPath());
    }


    @Test
    public void getAllOffers_success() {

        TestSubscriber<List<Offer>> testSubscriber = new TestSubscriber<>();
        offersRepository = new OffersRepository(assetManager, gson, resource.getPath());
        offersRepository.getAllOffers(true).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertComplete();
        assertThat(testSubscriber.values().get(0), hasSize(3));
        assertThat(testSubscriber.values().get(0).get(0).getId(), equalTo(FIRST_OFFER_ID));

        testSubscriber.dispose();
    }


    @Test
    public void getAllOffers_error() {
        TestSubscriber<List<Offer>> testSubscriber = new TestSubscriber<>();
        offersRepository = new OffersRepository(assetManager, gson, FAKE_JSON);
        offersRepository.getAllOffers(true).subscribe(testSubscriber);
        testSubscriber.assertError(Exception.class);

        testSubscriber.dispose();
    }
}
