package com.ibotta.ibottadevtest.offerslist;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerList.OffersListContract;
import com.ibotta.ibottadevtest.offerList.OffersListPresenter;
import com.ibotta.ibottadevtest.utils.RxSchedulers;
import com.ibotta.ibottadevtest.utils.TestSchedulers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OffersListPresenterTest {

    private static List<Offer> OFFERS;
    private static List<Offer> EMPTY_OFFERS;

    @Mock
    private OffersListContract.Model model;

    @Mock
    private OffersListContract.View view;

    private RxSchedulers rxSchedulers;

    private OffersListPresenter offersListPresenter;

    @Before
    public void setupTasksPresenter() {
        MockitoAnnotations.initMocks(this);

        OFFERS = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            OFFERS.add(createOffer(i));
        }

        EMPTY_OFFERS = new ArrayList<>();

        rxSchedulers = new TestSchedulers();

        offersListPresenter = new OffersListPresenter(view, model, rxSchedulers);
    }

    private Offer createOffer(int value) {
        Offer offer = new Offer();
        offer.setId(String.valueOf(value));
        offer.setName(String.valueOf(value));
        offer.setCurrentValue(String.valueOf(value));
        offer.setDescription(String.valueOf(value));
        offer.setTerms(String.valueOf(value));
        offer.setUrl(String.valueOf(value));

        return offer;
    }


    @Test
    public void createPresenter_setsThePresenterToView() {
        verify(view).setPresenter(offersListPresenter);
    }


    @Test
    public void loadAllOffersFromModelAndLoadIntoView() {
        when(model.provideOffersList(true)).thenReturn(Flowable.just(OFFERS));

        offersListPresenter.loadOffers(true);

        verify(view).setLoadingIndicator(true);
        verify(view).setLoadingIndicator(false);
    }

    @Test
    public void noOffers_showNoOffers() {
        when(model.provideOffersList(true)).thenReturn(Flowable.just(EMPTY_OFFERS));

        offersListPresenter.loadOffers(true);

        verify(view).showNoOffers();
    }


    @Test
    public void errorLoadingOffers_ShowsError() {
        when(model.provideOffersList(true)).thenReturn(Flowable.error(new Exception()));

        offersListPresenter.loadOffers(true);

        verify(view).showLoadingOffersError();
    }
}
