package com.ibotta.ibottadevtest.offerdetails;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsContract;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsPresenter;
import com.ibotta.ibottadevtest.utils.RxSchedulers;
import com.ibotta.ibottadevtest.utils.TestSchedulers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OfferDetailsPresenterTest {

    public static final Offer OFFER = createOffer(0);

    @Mock
    private OfferDetailsContract.Model model;

    @Mock
    private OfferDetailsContract.View view;

    private RxSchedulers rxSchedulers;

    private OfferDetailsPresenter offerDetailsPresenter;


    private static Offer createOffer(int value) {
        Offer offer = new Offer();
        offer.setId(String.valueOf(value));
        offer.setName(String.valueOf(value));
        offer.setCurrentValue(String.valueOf(value));
        offer.setDescription(String.valueOf(value));
        offer.setTerms(String.valueOf(value));
        offer.setUrl(String.valueOf(value));

        return offer;
    }


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        rxSchedulers = new TestSchedulers();
    }


    @Test
    public void createPresenter_setsThePresenterToView() {
        offerDetailsPresenter = new OfferDetailsPresenter(view, model, rxSchedulers);
        verify(view).setPresenter(offerDetailsPresenter);
    }


    @Test
    public void getOfferFromRepositoryAndLoadIntoView() {
        offerDetailsPresenter = new OfferDetailsPresenter(view, model, rxSchedulers);
        setOfferAvailable(OFFER);
        offerDetailsPresenter.subscribe();

        verify(view).loadImage(OFFER.getUrl());
        verify(view).setAmount(OFFER.getCurrentValue());
        verify(view).setName(OFFER.getName());
        verify(view).setDescription(OFFER.getDescription());
        verify(view).setTerms(OFFER.getTerms());
    }


    @Test
    public void toggleOfferFavorite() {
        offerDetailsPresenter = new OfferDetailsPresenter(view, model, rxSchedulers);
        setOfferAvailable(OFFER);

        offerDetailsPresenter.toggleFavorite();

        verify(model).toggleFavorite();
        verify(view).invalidateMenu();
    }


    private void setOfferAvailable(Offer offer) {
        when(model.provideOffer()).thenReturn(offer);
    }

}
