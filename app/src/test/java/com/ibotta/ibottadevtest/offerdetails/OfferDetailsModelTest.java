package com.ibotta.ibottadevtest.offerdetails;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsModel;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class OfferDetailsModelTest {

    public static final Offer OFFER = createOffer(0);

    @Mock
    FavoritesRepository repository;

    OfferDetailsModel model;


    private static Offer createOffer(int value) {
        Offer offer = new Offer();
        offer.setId(String.valueOf(value));
        offer.setName(String.valueOf(value));
        offer.setCurrentValue(String.valueOf(value));
        offer.setDescription(String.valueOf(value));
        offer.setTerms(String.valueOf(value));
        offer.setUrl(String.valueOf(value));

        return offer;
    }


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void toggleFavorites() {
        model = new OfferDetailsModel(OFFER, repository);

        model.toggleFavorite();
        verify(repository).isFavoriteOffer(OFFER.getId());
        verify(repository).insertOfferIntoFavorites(OFFER);
    }


}
