package com.ibotta.ibottadevtest.db.favoriteoffers;

import com.ibotta.ibottadevtest.model.FavoriteOffer;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;

@Dao
public interface FavoriteOfferDAO {

    @Query("SELECT * FROM favorite_offers")
    Single<List<FavoriteOffer>> getAllFavouriteOffers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FavoriteOffer favoriteOffer);

    @Delete
    void remove(FavoriteOffer favoriteOffer);

    @Query("SELECT count(*) FROM favorite_offers where id LIKE :id")
    int isFavoriteOffer(String id);
}
