package com.ibotta.ibottadevtest.db;

import com.ibotta.ibottadevtest.db.favoriteoffers.FavoriteOfferDAO;
import com.ibotta.ibottadevtest.model.FavoriteOffer;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {FavoriteOffer.class}, version = 1, exportSchema = false)
public abstract class OffersDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "offers.db";


    public abstract FavoriteOfferDAO favoriteOffersDao();
}
