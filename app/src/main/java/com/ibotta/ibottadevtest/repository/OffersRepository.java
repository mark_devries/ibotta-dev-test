package com.ibotta.ibottadevtest.repository;

import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ibotta.ibottadevtest.model.Offer;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class OffersRepository {

    private static final String TAG = OffersRepository.class.getSimpleName();

    public static final String OFFERS_DATA_FILE = "Offers.json";

    private AssetManager assetManager;
    private Gson gson;
    private String dataPath;

    @VisibleForTesting
    @Nullable
    List<Offer> cachedOffers;


    public OffersRepository(AssetManager assetManager, Gson gson, String dataPath) {
        this.assetManager = assetManager;
        this.gson = gson;
        this.dataPath = dataPath;
    }

    public Flowable<List<Offer>> getAllOffers(boolean force) {

        if (cachedOffers != null && !force) {
            return Flowable.fromIterable(cachedOffers).toList().toFlowable();
        }

        List<Offer> offers = null;
        try {
            offers = parseOffers();
            cachedOffers = offers;
        } catch (Exception e) {
            return Flowable.error(e);
        }
        return Flowable.fromIterable(offers).toList().toFlowable();
    }


    private List<Offer> parseOffers() throws Exception {
        InputStream is = null;
        try {
            is = assetManager.open(dataPath);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            String jsonStr = new String(buffer, "UTF-8");
            Type listType = new TypeToken<List<Offer>>() {
            }.getType();
            List<Offer> offersList = gson.fromJson(jsonStr, listType);
            return offersList;
        } finally {
            if (is != null) {
                is.close();
            }

        }
    }

    public Observable<List<Offer>> getOffersWithQuery(String query) {
        String lowerCasedQuery = query.toLowerCase();
        if (cachedOffers != null) {
            List<Offer> offerList = new ArrayList<>();
            for (Offer offer : cachedOffers) {
                if (offer.getName() != null && offer.getName().toLowerCase().contains(lowerCasedQuery)) {
                    offerList.add(offer);
                } else if (offer.getCurrentValue() != null && offer.getCurrentValue().toLowerCase().contains(lowerCasedQuery)) {
                    offerList.add(offer);
                }
            }

            return Observable.just(offerList);
        }

        return Observable.just(new ArrayList<>());
    }
}
