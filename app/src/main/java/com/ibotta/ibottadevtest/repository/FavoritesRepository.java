package com.ibotta.ibottadevtest.repository;

import com.ibotta.ibottadevtest.db.favoriteoffers.FavoriteOfferDAO;
import com.ibotta.ibottadevtest.model.FavoriteOffer;
import com.ibotta.ibottadevtest.model.Offer;

import java.util.List;

import io.reactivex.Single;

public class FavoritesRepository {

    private final FavoriteOfferDAO favoriteOfferDao;


    public FavoritesRepository(FavoriteOfferDAO dao) {
        this.favoriteOfferDao = dao;
    }

    public Single<List<FavoriteOffer>> getAllFavoriteOffers() {
        return favoriteOfferDao.getAllFavouriteOffers();
    }

    public boolean isFavoriteOffer(String id) {
        return favoriteOfferDao.isFavoriteOffer(id) > 0;
    }

    public void insertOfferIntoFavorites(Offer offer) {
        FavoriteOffer favoriteOffer = new FavoriteOffer();
        favoriteOffer.setId(offer.getId());
        favoriteOffer.setCurrentValue(offer.getCurrentValue());
        favoriteOffer.setDescription(offer.getDescription());
        favoriteOffer.setName(offer.getName());
        favoriteOffer.setTerms(offer.getTerms());
        favoriteOffer.setUrl(offer.getUrl());

        insertIntoFavorites(favoriteOffer);
    }

    public void removeOfferFromFavorites(Offer offer) {
        FavoriteOffer favoriteOffer = new FavoriteOffer();
        favoriteOffer.setId(offer.getId());
        favoriteOffer.setCurrentValue(offer.getCurrentValue());
        favoriteOffer.setDescription(offer.getDescription());
        favoriteOffer.setName(offer.getName());
        favoriteOffer.setTerms(offer.getTerms());
        favoriteOffer.setUrl(offer.getUrl());

        removeFromFavorites(favoriteOffer);
    }

    public void insertIntoFavorites(FavoriteOffer favoriteOffer) {
        favoriteOfferDao.insert(favoriteOffer);
    }

    public void removeFromFavorites(FavoriteOffer favoriteOffer) {
        favoriteOfferDao.remove(favoriteOffer);
    }
}
