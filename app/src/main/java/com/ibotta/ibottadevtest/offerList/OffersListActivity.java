package com.ibotta.ibottadevtest.offerList;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.application.OffersApplication;
import com.ibotta.ibottadevtest.offerList.dagger.DaggerOffersListComponent;
import com.ibotta.ibottadevtest.offerList.dagger.OffersListModule;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class OffersListActivity extends AppCompatActivity {

    private static final String TAG = OffersListActivity.class.getSimpleName();

    @Inject
    OffersListContract.View view;

    @Inject
    OffersListContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerOffersListComponent.builder()
                .appComponent(OffersApplication.getAppComponent())
                .offersListModule(new OffersListModule(this))
                .build()
                .inject(this);

        presenter.subscribe();
    }


    @Override
    protected void onResume() {
        super.onResume();
        view.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unsubscribe();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_offers_list, menu);
        view.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            view.onRefreshClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
