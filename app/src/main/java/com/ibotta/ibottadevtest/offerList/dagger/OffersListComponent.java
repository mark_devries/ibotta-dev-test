package com.ibotta.ibottadevtest.offerList.dagger;

import com.ibotta.ibottadevtest.application.dagger.AppComponent;
import com.ibotta.ibottadevtest.offerList.OffersListActivity;

import dagger.Component;

@OffersListScope
@Component(dependencies = {AppComponent.class}, modules = {OffersListModule.class})
public interface OffersListComponent {

    void inject(OffersListActivity offersListActivity);
}
