package com.ibotta.ibottadevtest.offerList;

import com.ibotta.ibottadevtest.model.FavoriteOffer;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;
import com.ibotta.ibottadevtest.repository.OffersRepository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class OffersListModel implements OffersListContract.Model {


    OffersRepository offersRepository;
    FavoritesRepository favoritesRespository;


    public OffersListModel(OffersRepository offfersRepository, FavoritesRepository favoritesRepository) {
        this.offersRepository = offfersRepository;
        this.favoritesRespository = favoritesRepository;
    }

    @Override
    public Flowable<List<Offer>> provideOffersList(boolean force) {
        return offersRepository.getAllOffers(force);
    }

    @Override
    public Observable<List<Offer>> provideOffersList(String query) {
        return offersRepository.getOffersWithQuery(query);
    }

    @Override
    public Single<List<FavoriteOffer>> provideFavoritesList() {
        return favoritesRespository.getAllFavoriteOffers();
    }

    @Override
    public boolean provideIsFavorite(Offer offer) {
        return favoritesRespository.isFavoriteOffer(offer.getId());
    }
}
