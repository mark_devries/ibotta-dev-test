package com.ibotta.ibottadevtest.offerList;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.ui.FavoritesFlagView;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

public class OfferListItemViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = OfferListItemViewHolder.class.getSimpleName();

    View view;

    Offer offer;

    @BindView(R.id.offer_image)
    ImageView offerImage;

    @BindView(R.id.offer_name)
    TextView offerName;

    @BindView(R.id.offer_amount)
    TextView offerAmount;

    @BindView(R.id.favorite_flag)
    FavoritesFlagView favoritesFlag;


    public OfferListItemViewHolder(@NonNull View itemView, final PublishSubject<Offer> clickSubject) {
        super(itemView);

        this.view = itemView;
        ButterKnife.bind(this, view);

        itemView.setOnClickListener(view -> {
            clickSubject.onNext(this.offer);
        });
    }

    public void bind(@NonNull Offer offer, boolean isFavorite) {
        this.offer = offer;
        setImageUrl(offer.getUrl());
        setName(offer.getName());
        setAmount(offer.getCurrentValue());
        setIsFavorite(isFavorite);
    }


    private void setImageUrl(String url) {
        offerImage.setImageResource(android.R.color.transparent);

        if (url == null || url.isEmpty()) {
            offerImage.setImageDrawable(offerImage.getContext().getDrawable(R.drawable.ic_no_image));
            offerImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            offerImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Picasso.with(offerImage.getContext()).load(url).error(R.drawable.ic_no_image).into(offerImage);
        }

    }


    private void setName(String name) {
        offerName.setText(name);
    }


    private void setAmount(String amount) {
        offerAmount.setText(amount);
    }


    private void setIsFavorite(boolean isFavorite) {
        favoritesFlag.setVisibility(isFavorite ? View.VISIBLE : View.GONE);
    }


}
