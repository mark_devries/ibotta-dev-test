package com.ibotta.ibottadevtest.offerList.dagger;

import com.ibotta.ibottadevtest.offerList.OffersListActivity;
import com.ibotta.ibottadevtest.offerList.OffersListContract;
import com.ibotta.ibottadevtest.offerList.OffersListModel;
import com.ibotta.ibottadevtest.offerList.OffersListPresenter;
import com.ibotta.ibottadevtest.offerList.OffersListView;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;
import com.ibotta.ibottadevtest.repository.OffersRepository;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class OffersListModule {

    OffersListActivity offersListContext;


    public OffersListModule(OffersListActivity context) {
        this.offersListContext = context;
    }


    @OffersListScope
    @Provides
    OffersListContract.View provideView() {
        return new OffersListView(offersListContext);
    }

    @OffersListScope
    @Provides
    OffersListContract.Presenter providePresenter(OffersListContract.View view, OffersListContract.Model model, RxSchedulers schedulers) {
        return new OffersListPresenter(view, model, schedulers);
    }

    @OffersListScope
    @Provides
    OffersListActivity provideContext() {
        return offersListContext;
    }

    @OffersListScope
    @Provides
    OffersListContract.Model provideModel(OffersRepository offersRepository, FavoritesRepository favoritesRepository) {
        return new OffersListModel(offersRepository, favoritesRepository);
    }
}
