package com.ibotta.ibottadevtest.offerList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.model.Offer;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class OffersListAdapter extends RecyclerView.Adapter<OfferListItemViewHolder> {

    private final PublishSubject<Offer> offerClickSubject = PublishSubject.create();
    private OffersListContract.Presenter presenter;
    private List<Offer> offerList = new ArrayList<>();

    public Observable<Offer> offerClickSubject() {
        return offerClickSubject;
    }

    public OffersListAdapter(OffersListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public OfferListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_offer_list_item, parent, false);
        return new OfferListItemViewHolder(view, offerClickSubject);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferListItemViewHolder offerViewHolder, int position) {
        Offer offer = offerList.get(position);
        offerViewHolder.bind(offer, presenter.getIsOfferFavorite(offer));
    }

    @Override
    public int getItemCount() {
        return (offerList != null) ? offerList.size() : 0;
    }


    public void replaceData(List<Offer> offerList) {
        this.offerList = offerList;
        notifyDataSetChanged();
    }
}
