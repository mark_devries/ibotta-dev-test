package com.ibotta.ibottadevtest.offerList;

import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsActivity;
import com.ibotta.ibottadevtest.ui.GridLayoutSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class OffersListView implements OffersListContract.View {

    private static final String TAG = OffersListView.class.getSimpleName();

    @BindView(R.id.offers_toolbar)
    Toolbar toolbar;

    @BindView(R.id.offers_list_recycleview)
    RecyclerView recyclerView;

    @BindView(R.id.offers_loading_progress)
    ContentLoadingProgressBar progressBar;

    @BindView(R.id.no_offers_view)
    View noOffersView;

    @BindView(R.id.offers_error_view)
    View offersErrorView;

    private OffersListActivity context;

    private OffersListAdapter adapter;

    private View view;

    private OffersListContract.Presenter presenter;

    private PublishSubject<String> searchSubject = PublishSubject.create();


    public OffersListView(OffersListActivity context) {
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.activity_offers_list, null, false);
        ButterKnife.bind(this, view);

        Resources res = context.getResources();

        int numColumns = res.getInteger(R.integer.offers_list_columns);
        int horizontalSpacing = res.getDimensionPixelSize(R.dimen.offerListItemHorizontalSpacing);
        int verticalSpacing = res.getDimensionPixelOffset(R.dimen.offerListItemVerticalSpacing);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, numColumns);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new GridLayoutSpaceItemDecoration(numColumns, horizontalSpacing, verticalSpacing, false));

        context.setContentView(view);
        context.setSupportActionBar(toolbar);

    }


    private void initAdapter() {
        adapter = new OffersListAdapter(presenter);
        recyclerView.setAdapter(adapter);
    }


    private void initSearchView(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e(TAG, "onQueryTextChange : " + newText);
                searchSubject.onNext(newText);
                return false;
            }
        });
    }


    @Override
    public void onResume() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        initSearchView(searchView);
    }

    @Override
    public Observable<Offer> offerClickSubject() {
        return adapter.offerClickSubject();
    }


    @Override
    public Observable<String> searchSubject() {
        return searchSubject;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showOffers(List<Offer> offers) {
        adapter.replaceData(offers);
        showRecyclerView(true);
        showNoOffersView(false);
        showOffersErrorView(false);
    }

    @Override
    public void showOfferDetails(Offer offer) {
        Intent intent = OfferDetailsActivity.getIntent(view.getContext(), offer);
        view.getContext().startActivity(intent);
    }

    @Override
    public void showNoOffers() {
        showRecyclerView(false);
        showNoOffersView(true);
        showOffersErrorView(false);
    }

    @Override
    public void showLoadingOffersError() {
        showRecyclerView(false);
        showNoOffersView(false);
        showOffersErrorView(true);
    }

    @Override
    public void refreshOffers() {
        adapter.replaceData(new ArrayList<>());
        showRecyclerView(false);
        showNoOffersView(false);
        showOffersErrorView(false);
        recyclerView.getLayoutManager().scrollToPosition(0);
    }

    @Override
    public void setPresenter(OffersListContract.Presenter presenter) {
        this.presenter = presenter;

        initAdapter();
    }


    @Override
    public void onRefreshClicked() {
        presenter.loadOffers(true);
    }

    private void showRecyclerView(boolean show) {
        recyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showNoOffersView(boolean show) {
        noOffersView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showOffersErrorView(boolean show) {
        offersErrorView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
