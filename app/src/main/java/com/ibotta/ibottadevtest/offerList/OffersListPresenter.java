package com.ibotta.ibottadevtest.offerList;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class OffersListPresenter implements OffersListContract.Presenter {

    private static final String TAG = OffersListPresenter.class.getSimpleName();

    @NonNull
    private final OffersListContract.View view;

    @NonNull
    private final OffersListContract.Model model;

    @NonNull
    private final RxSchedulers rxSchedulers;

    List<Offer> offerList = new ArrayList<>();

    private boolean firstLoad = true;

    PublishSubject<Flowable<List<Offer>>> publishSubject = null;

    @NonNull
    private CompositeDisposable compositeDisposable;

    private Disposable loadOffersDisposable;

    public OffersListPresenter(OffersListContract.View view, OffersListContract.Model model, RxSchedulers schedulers) {
        this.view = view;
        this.model = model;
        this.rxSchedulers = schedulers;

        compositeDisposable = new CompositeDisposable();

        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        compositeDisposable.clear();
        subscribeOfferClick();
        subscribeSearch();
        loadOffers(false);
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }


    @Override
    public void loadOffers(boolean forceUpdate) {
        loadOffers(forceUpdate || firstLoad, true);
        firstLoad = false;
    }

    private void loadOffers(final boolean forceUpdate, final boolean showLoadingUI) {
        if (showLoadingUI) {
            view.setLoadingIndicator(true);
        }
        if (forceUpdate) {
            view.refreshOffers();
        }

        //compositeDisposable.clear();
        if (loadOffersDisposable != null) {
            compositeDisposable.remove(loadOffersDisposable);
        }
        loadOffersDisposable = model.provideOffersList(forceUpdate)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidThread())
                .subscribe(
                        // onNext
                        offers -> {

                            if (!offers.isEmpty()) {
                                view.showOffers(offers);
                            } else {
                                view.showNoOffers();
                            }

                            view.setLoadingIndicator(false);
                        },
                        // onError
                        throwable -> {
                            view.showLoadingOffersError();
                            view.setLoadingIndicator(false);
                        });

        compositeDisposable.add(loadOffersDisposable);
    }

    @Override
    public boolean getIsOfferFavorite(@NonNull Offer offer) {
        return model.provideIsFavorite(offer);
    }

    private void subscribeOfferClick() {
        compositeDisposable.add(view.offerClickSubject().subscribe(offer -> {
            view.showOfferDetails(offer);
        }));
    }

    private void subscribeSearch() {
        compositeDisposable.add(view.searchSubject()
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap(query -> model.provideOffersList(query)
                        .subscribeOn(rxSchedulers.io())
                        .observeOn(rxSchedulers.androidThread()))
                .subscribe(offerList -> {
                    if (offerList.isEmpty()) {
                        view.showNoOffers();
                    } else {
                        view.showOffers(offerList);
                    }
                }, throwable -> {
                    view.showLoadingOffersError();
                })
        );
    }
}
