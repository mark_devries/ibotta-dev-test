package com.ibotta.ibottadevtest.offerList;

import android.view.Menu;

import com.ibotta.ibottadevtest.model.FavoriteOffer;
import com.ibotta.ibottadevtest.model.Offer;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class OffersListContract {


    public interface View {

        void setLoadingIndicator(boolean active);

        void showOffers(List<Offer> offers);

        void showOfferDetails(Offer offer);

        void showNoOffers();

        void showLoadingOffersError();

        void refreshOffers();

        void setPresenter(Presenter presenter);

        Observable<Offer> offerClickSubject();

        Observable<String> searchSubject();

        void onResume();

        void onRefreshClicked();

        void onCreateOptionsMenu(Menu menu);
    }


    public interface Presenter {

        void subscribe();

        void unsubscribe();

        void loadOffers(boolean forceUpdate);

        boolean getIsOfferFavorite(@NonNull Offer offer);
    }


    public interface Model {

        Flowable<List<Offer>> provideOffersList(boolean force);

        Observable<List<Offer>> provideOffersList(String query);

        Single<List<FavoriteOffer>> provideFavoritesList();

        boolean provideIsFavorite(Offer offer);
    }
}
