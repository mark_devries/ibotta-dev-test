package com.ibotta.ibottadevtest.offerDetails;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.model.Offer;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferDetailsView implements OfferDetailsContract.View {

    OfferDetailsActivity context;
    private OfferDetailsContract.Presenter presenter;
    private View view;

    @BindView(R.id.offers_toolbar)
    Toolbar toolbar;

    @BindView(R.id.offer_image)
    ImageView offerImage;

    @BindView(R.id.offer_amount)
    TextView offerAmount;

    @BindView(R.id.offer_name)
    TextView offerName;

    @BindView(R.id.offer_description)
    TextView offerDescription;

    @BindView(R.id.offer_terms)
    TextView offerTerms;

    public OfferDetailsView(OfferDetailsActivity context) {
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.activity_offer_details, null, false);
        ButterKnife.bind(this, view);

        context.setContentView(view);

        context.setSupportActionBar(toolbar);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    public View view() {
        return view;
    }

    @Override
    public void setPresenter(OfferDetailsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void loadImage(String url) {
        if (url == null || url.isEmpty()) {
            offerImage.setImageDrawable(offerImage.getContext().getDrawable(R.drawable.ic_no_image));
            offerImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            offerImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Picasso.with(offerImage.getContext()).load(url).error(R.drawable.ic_no_image).fit().centerInside().into(offerImage);
        }
    }

    @Override
    public void setAmount(String amount) {
        offerAmount.setText(amount);
    }

    @Override
    public void setName(String name) {
        offerName.setText(name);
    }

    @Override
    public void setDescription(String description) {
        offerDescription.setText(description);
    }

    @Override
    public void setTerms(String terms) {
        offerTerms.setText(terms);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu) {
        new MenuInflater(view.getContext()).inflate(R.menu.activity_offer_details, menu);
        updateMenuDrawables(menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        updateMenuDrawables(menu);
    }

    @Override
    public void updateMenuDrawables(Menu menu) {
        boolean isFavorite = presenter.getIsFavorite();
        MenuItem menuItem = menu.findItem(R.id.menu_favorite);
        if (isFavorite) {
            menuItem.setIcon(R.drawable.ic_star_gold);
        } else {
            menuItem.setIcon(R.drawable.ic_star_white);
        }
    }

    @Override
    public void invalidateMenu() {
        context.invalidateOptionsMenu();
    }

    @Override
    public void favoriteButtonClicked() {
        presenter.toggleFavorite();
    }

    @Override
    public void onSupportNavigateUp() {
        context.onBackPressed();
    }
}
