package com.ibotta.ibottadevtest.offerDetails.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.CLASS)
@interface OfferDetailsScope {
}
