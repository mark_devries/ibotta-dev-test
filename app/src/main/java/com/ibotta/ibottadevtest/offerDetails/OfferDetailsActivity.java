package com.ibotta.ibottadevtest.offerDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ibotta.ibottadevtest.R;
import com.ibotta.ibottadevtest.application.OffersApplication;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerDetails.dagger.DaggerOfferDetailsComponent;
import com.ibotta.ibottadevtest.offerDetails.dagger.OfferDetailsModule;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class OfferDetailsActivity extends AppCompatActivity {


    private static final String TAG = OfferDetailsActivity.class.getSimpleName();

    public static final String EXTRA_OFFER = "extra_offer";


    @Inject
    OfferDetailsContract.View view;

    @Inject
    OfferDetailsContract.Presenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Offer offer = getIntent().getExtras().getParcelable(EXTRA_OFFER);

        if (offer == null) {
            finish();
            return;
        }

        DaggerOfferDetailsComponent.builder()
                .appComponent(OffersApplication.getAppComponent())
                .offerDetailsModule(new OfferDetailsModule(this, offer))
                .build()
                .inject(this);

        presenter.subscribe();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unsubscribe();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        view.onCreateOptionsMenu(menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        view.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_favorite) {
            view.favoriteButtonClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        view.onSupportNavigateUp();
        return true;
    }

    public static Intent getIntent(Context context, Offer offer) {
        Intent intent = new Intent(context, OfferDetailsActivity.class);
        intent.putExtra(EXTRA_OFFER, offer);
        return intent;
    }
}
