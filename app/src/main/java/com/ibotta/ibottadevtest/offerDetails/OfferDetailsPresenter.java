package com.ibotta.ibottadevtest.offerDetails;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import androidx.annotation.NonNull;
import io.reactivex.disposables.CompositeDisposable;

public class OfferDetailsPresenter implements OfferDetailsContract.Presenter {

    @NonNull
    private final OfferDetailsContract.View view;

    @NonNull
    private final OfferDetailsContract.Model model;

    @NonNull
    private final RxSchedulers rxSchedulers;

    @NonNull
    private CompositeDisposable compositeDisposable;


    public OfferDetailsPresenter(OfferDetailsContract.View view, OfferDetailsContract.Model model, RxSchedulers schedulers) {
        this.view = view;
        this.model = model;
        this.rxSchedulers = schedulers;

        compositeDisposable = new CompositeDisposable();

        this.view.setPresenter(this);
    }


    public void subscribe() {
        bindData();
    }


    public void unsubscribe() {
        compositeDisposable.clear();
    }

    private void bindData() {
        Offer offer = model.provideOffer();
        view.loadImage(offer.getUrl());
        view.setAmount(offer.getCurrentValue());
        view.setName(offer.getName());
        view.setDescription(offer.getDescription());
        view.setTerms(offer.getTerms());
    }

    @Override
    public void toggleFavorite() {
        this.model.toggleFavorite();
        this.view.invalidateMenu();
    }


    @Override
    public boolean getIsFavorite() {
        return model.getOfferIsFavorite();
    }
}
