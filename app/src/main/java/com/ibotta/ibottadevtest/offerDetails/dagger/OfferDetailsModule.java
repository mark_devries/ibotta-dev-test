package com.ibotta.ibottadevtest.offerDetails.dagger;

import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsActivity;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsContract;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsModel;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsPresenter;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsView;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class OfferDetailsModule {

    private OfferDetailsActivity offerDetailsContext;
    private Offer offer;

    public OfferDetailsModule(OfferDetailsActivity context, Offer offer) {
        this.offerDetailsContext = context;
        this.offer = offer;
    }


    @OfferDetailsScope
    @Provides
    OfferDetailsContract.View provideView() {
        return new OfferDetailsView(offerDetailsContext);
    }

    @OfferDetailsScope
    @Provides
    OfferDetailsContract.Presenter providePresenter(OfferDetailsContract.View view, OfferDetailsContract.Model model, RxSchedulers schedulers) {
        return new OfferDetailsPresenter(view, model, schedulers);
    }

    @OfferDetailsScope
    @Provides
    OfferDetailsActivity provideContext() {
        return offerDetailsContext;
    }

    @OfferDetailsScope
    @Provides
    OfferDetailsContract.Model provideModel(FavoritesRepository favoritesRepository) {
        return new OfferDetailsModel(offer, favoritesRepository);
    }
}
