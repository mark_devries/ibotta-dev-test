package com.ibotta.ibottadevtest.offerDetails.dagger;


import com.ibotta.ibottadevtest.application.dagger.AppComponent;
import com.ibotta.ibottadevtest.offerDetails.OfferDetailsActivity;

import dagger.Component;

@OfferDetailsScope
@Component(dependencies = {AppComponent.class}, modules = {OfferDetailsModule.class})
public interface OfferDetailsComponent {

    void inject(OfferDetailsActivity offerDetailsActivity);
}
