package com.ibotta.ibottadevtest.offerDetails;

import android.view.Menu;

import com.ibotta.ibottadevtest.model.FavoriteOffer;
import com.ibotta.ibottadevtest.model.Offer;

import java.util.List;

import io.reactivex.Single;

public class OfferDetailsContract {


    public interface View {
        void setPresenter(Presenter presenter);

        void invalidateMenu();

        void favoriteButtonClicked();

        void onCreateOptionsMenu(Menu menu);

        void onPrepareOptionsMenu(Menu menu);

        void updateMenuDrawables(Menu menu);

        void onSupportNavigateUp();

        void loadImage(String url);

        void setAmount(String amount);

        void setName(String name);

        void setDescription(String description);

        void setTerms(String terms);
    }

    public interface Presenter {

        void toggleFavorite();

        boolean getIsFavorite();

        void subscribe();

        void unsubscribe();
    }

    public interface Model {
        Offer provideOffer();

        Single<List<FavoriteOffer>> provideFavoritesList();

        void toggleFavorite();

        void insertOfferIntoFavorites();

        void removeOfferFromFavorites();

        boolean getOfferIsFavorite();
    }
}
