package com.ibotta.ibottadevtest.offerDetails;

import com.ibotta.ibottadevtest.model.FavoriteOffer;
import com.ibotta.ibottadevtest.model.Offer;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;

import java.util.List;

import io.reactivex.Single;

public class OfferDetailsModel implements OfferDetailsContract.Model {


    Offer offer;
    FavoritesRepository favoritesRespository;


    public OfferDetailsModel(Offer offer, FavoritesRepository favoritesRepository) {
        this.offer = offer;
        this.favoritesRespository = favoritesRepository;
    }


    @Override
    public Offer provideOffer() {
        return offer;
    }

    @Override
    public Single<List<FavoriteOffer>> provideFavoritesList() {
        return favoritesRespository.getAllFavoriteOffers();
    }

    @Override
    public void insertOfferIntoFavorites() {
        favoritesRespository.insertOfferIntoFavorites(offer);
    }

    @Override
    public void removeOfferFromFavorites() {
        favoritesRespository.removeOfferFromFavorites(offer);
    }


    @Override
    public void toggleFavorite() {
        if (getOfferIsFavorite()) {
            removeOfferFromFavorites();
        } else {
            insertOfferIntoFavorites();
        }
    }


    @Override
    public boolean getOfferIsFavorite() {
        return favoritesRespository.isFavoriteOffer(offer.getId());
    }
}

