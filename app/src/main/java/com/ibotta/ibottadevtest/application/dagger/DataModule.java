package com.ibotta.ibottadevtest.application.dagger;

import android.content.Context;

import com.google.gson.Gson;
import com.ibotta.ibottadevtest.db.OffersDatabase;
import com.ibotta.ibottadevtest.db.favoriteoffers.FavoriteOfferDAO;
import com.ibotta.ibottadevtest.repository.FavoritesRepository;
import com.ibotta.ibottadevtest.repository.OffersRepository;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {


    @AppScope
    @Provides
    Gson provideGsonClient() {
        return new Gson();
    }

    @AppScope
    @Provides
    OffersRepository provideOffersRepository(Context context, Gson gson) {
        return new OffersRepository(context.getAssets(), gson, OffersRepository.OFFERS_DATA_FILE);
    }

    @AppScope
    @Provides
    public OffersDatabase provideOffersDatabase(Context context) {
        return Room.databaseBuilder(context,
                OffersDatabase.class, OffersDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @AppScope
    @Provides
    public FavoriteOfferDAO provideShowDao(OffersDatabase offersDatabase) {
        return offersDatabase.favoriteOffersDao();
    }

    @AppScope
    @Provides
    FavoritesRepository provideFavoritesRepository(FavoriteOfferDAO dao) {
        return new FavoritesRepository(dao);
    }
}
