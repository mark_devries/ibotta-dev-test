package com.ibotta.ibottadevtest.application;

import android.app.Application;

import com.ibotta.ibottadevtest.application.dagger.AppComponent;
import com.ibotta.ibottadevtest.application.dagger.AppContextModule;
import com.ibotta.ibottadevtest.application.dagger.DaggerAppComponent;

public class OffersApplication extends Application {

    private static AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }


    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }


    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
