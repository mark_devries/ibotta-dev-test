package com.ibotta.ibottadevtest.application.dagger;

import com.ibotta.ibottadevtest.utils.AppSchedulers;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppSchedulers();
    }
}
