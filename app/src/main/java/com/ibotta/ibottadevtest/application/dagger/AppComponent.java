package com.ibotta.ibottadevtest.application.dagger;

import com.ibotta.ibottadevtest.repository.FavoritesRepository;
import com.ibotta.ibottadevtest.repository.OffersRepository;
import com.ibotta.ibottadevtest.utils.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {DataModule.class, AppContextModule.class, RxModule.class})
public interface AppComponent {

    RxSchedulers rxSchedulers();

    OffersRepository offersRepository();

    FavoritesRepository favoritesRepository();

}
