package com.ibotta.ibottadevtest.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.ibotta.ibottadevtest.R;

import androidx.annotation.ColorInt;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

public class FavoritesFlagView extends FrameLayout {

    private Drawable favoriteDrawable;

    @ColorInt
    private int backgroundColor = Color.parseColor("#000000");

    private Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Path backgroundPath = new Path();

    private float cornerRadius = 0;


    public FavoritesFlagView(Context context) {
        this(context, null);
    }

    public FavoritesFlagView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoritesFlagView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defaultStyleAttr) {
        favoriteDrawable = VectorDrawableCompat.create(getContext().getResources(), R.drawable.ic_star_gold, null);
        backgroundPaint.setColor(backgroundColor);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FavoritesFlagView,
                defaultStyleAttr, 0);

        try {
            cornerRadius = a.getDimensionPixelSize(R.styleable.FavoritesFlagView_cornerRadius, 0);
        } finally {
            a.recycle();
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        initBackgroundPath();
        setStarBounds();
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        initBackgroundPath();
        setStarBounds();
    }

    public void setBackgroundColor(@ColorInt int color) {
        backgroundColor = color;
        backgroundPaint.setColor(backgroundColor);
        initBackgroundPath();
        setStarBounds();
        invalidate();
    }

    public void setDrawableColor(@ColorInt int color) {
        favoriteDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        setStarBounds();
        invalidate();
    }


    public void setCornerRadius(float cornerRadius) {
        this.cornerRadius = cornerRadius;
        initBackgroundPath();
        invalidate();
    }


    private void initBackgroundPath() {
        backgroundPath.reset();
        backgroundPath.moveTo(this.getWidth(), 0);
        backgroundPath.lineTo(0, this.getHeight());
        backgroundPath.lineTo(this.getWidth() - cornerRadius, this.getHeight());
        backgroundPath.cubicTo(this.getWidth() - cornerRadius, this.getHeight(), this.getWidth(), this.getHeight(), this.getWidth(), this.getHeight() - cornerRadius);
        backgroundPath.lineTo(this.getWidth(), 0);
        backgroundPath.close();
    }


    private void setStarBounds() {
        int drawableWidth = favoriteDrawable.getIntrinsicWidth();
        int drawableHeight = favoriteDrawable.getIntrinsicHeight();
        int quarterWidth = drawableWidth / 4;
        int quarterHeight = drawableHeight / 4;

        Rect rect = new Rect();
        rect.top = getHeight() / 2;
        rect.left = getWidth() / 2;
        rect.right = getWidth() - quarterWidth;
        rect.bottom = getHeight() - quarterHeight;

        favoriteDrawable.setBounds(rect);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawPath(backgroundPath, backgroundPaint);
        favoriteDrawable.draw(canvas);
    }
}
