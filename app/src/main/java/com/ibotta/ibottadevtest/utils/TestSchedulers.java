package com.ibotta.ibottadevtest.utils;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class TestSchedulers implements RxSchedulers {

    @Override
    public Scheduler runOnBackground() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler compute() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler androidThread() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler internet() {
        return Schedulers.trampoline();
    }
}
