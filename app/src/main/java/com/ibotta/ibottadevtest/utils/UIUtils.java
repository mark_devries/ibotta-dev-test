package com.ibotta.ibottadevtest.utils;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.ibotta.ibottadevtest.R;

public class UIUtils {


    public static void showSnackbarError(View view, String message, int length) {
        Snackbar.make(view, message, length)
                .setActionTextColor(view.getContext().getColor(R.color.colorError))
                .setAction("OK", null)
                .show();
    }
}
