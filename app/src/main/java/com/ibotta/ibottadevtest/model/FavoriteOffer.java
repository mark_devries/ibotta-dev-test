package com.ibotta.ibottadevtest.model;

import androidx.room.Entity;

@Entity(tableName = "favorite_offers")
public class FavoriteOffer extends Offer {


    private boolean favorite;


    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof FavoriteOffer && ((FavoriteOffer) obj).getId() == getId();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
