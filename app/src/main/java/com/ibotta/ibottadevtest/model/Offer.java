package com.ibotta.ibottadevtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.PrimaryKey;

public class Offer implements Parcelable {

    @PrimaryKey
    @NonNull
    private String id;

    @Nullable
    private String url;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String terms;

    @SerializedName("current_value")
    @Nullable
    private String currentValue;


    public Offer() {
    }

    protected Offer(Parcel in) {
        id = in.readString();
        url = in.readString();
        name = in.readString();
        description = in.readString();
        terms = in.readString();
        currentValue = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(url);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(terms);
        parcel.writeString(currentValue);
    }


    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel in) {
            return new Offer(in);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };
}
